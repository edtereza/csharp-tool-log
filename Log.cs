﻿using System;
using System.IO;
using System.Reflection;
using System.Globalization;

namespace Tools
{
    public static class Log
    {
        public enum LogLevel
        {
            Critical = 0
            , Error = 1
            , Information = 2
            , Debug = 3
        }
        public static Boolean Enabled = true;
        public static LogLevel Level = LogLevel.Information;
        public static String Path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        public static String File = Assembly.GetEntryAssembly().GetName().Name;
        private static StreamWriter StreamWriter;
        private static void Critical(String Message)
        {
            Message = "CRITICAL :: " + Message;
            Log.Write(Message);
        }
        public static void Critical(MethodBase MethodBase, String Message)
        {
            if (Message == "-")
                Message = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
            else if (Message == "#")
                Message = "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #";
            Log.Critical(Log.MethodBase(MethodBase), Message);
        }
        public static void Critical(String Source, String Message)
        {
            if (Source != null)
            {
                if (Message == "-")
                    Message = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
                else if (Message == "#")
                    Message = "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #";
                Message = Source + " :: " + Message;
            }
            Log.Critical(Message);
        }
        private static void Error(String Message)
        {
            if (Log.Level >= LogLevel.Error)
            {
                Message = "ERROR :: " + Message;
                Log.Write(Message);
            }
        }
        public static void Error(MethodBase MethodBase, String Message)
        {
            if (Message == "-")
                Message = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
            else if (Message == "#")
                Message = "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #";
            Log.Error(Log.MethodBase(MethodBase), Message);
        }
        public static void Error(String Source, String Message)
        {
            if (Source != null)
            {
                if (Message == "-")
                    Message = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
                else if (Message == "#")
                    Message = "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #";
                Message = Source + " :: " + Message;
            }
            Log.Error(Message);
        }
        private static void Information(String Message)
        {
            if (Log.Level >= LogLevel.Information)
            {
                Message = "INFO :: " + Message;
                Log.Write(Message);
            }
        }
        public static void Information(MethodBase MethodBase, String Message)
        {
            if (Message == "-")
                Message = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
            else if (Message == "#")
                Message = "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #";
            Log.Information(Log.MethodBase(MethodBase), Message);
        }
        public static void Information(String Source, String Message)
        {
            if (Source != null)
            {
                if (Message == "-")
                    Message = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
                else if (Message == "#")
                    Message = "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #";
                Message = Source + " :: " + Message;
            }
            Log.Information(Message);
        }
        private static void Debug(String Message)
        {
            if (Log.Level >= LogLevel.Debug)
            {
                Message = "DEBUG :: " + Message;
                Log.Write(Message);
            }
        }
        public static void Debug(MethodBase MethodBase, String Message)
        {
            if (Message == "-")
                Message = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
            else if (Message == "#")
                Message = "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #";
            Log.Debug(Log.MethodBase(MethodBase), Message);
        }
        public static void Debug(String Source, String Message)
        {
            if (Source != null)
            {
                if (Message == "-")
                    Message = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
                else if (Message == "#")
                    Message = "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #";
                Message = Source + " :: " + Message;
            }
            Log.Debug(Message);
        }
        public static void Exception(Exception Exception)
        {
            String _Excecao = "EXCEPTION :: " + Exception.Message;
            Log.Write(_Excecao);
            Log.Write("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
            Log.Write(Exception.ToString());
            Log.Write("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        }
        public static void Exception(MethodBase MethodBase, Exception Exception)
        {
            Log.Exception(Log.MethodBase(MethodBase), Exception);
        }
        public static void Exception(String Source, Exception Exception)
        {
            String _STR_Message = "EXCEPTION :: " + Source + " :: " + Exception.Message;
            Log.Write(_STR_Message);
            Log.Write("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
            Log.Write(Exception.ToString());
            Log.Write("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
        }
        private static Boolean OpenWriter()
        {
            String _Path = Log.Path;
            if (_Path == null || !Directory.Exists(_Path.Trim()))
            {
                _Path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            }
            if (!_Path.EndsWith(@"\"))
            {
                _Path = _Path + @"\";
            }
            String _File = Log.File;
            if (_File == null || _File.Trim().Length == 0)
            {
                _File = Assembly.GetEntryAssembly().GetName().Name;
            }
            if (!_File.EndsWith(@".log"))
            {
                _File = _File + @".log";
            }
            try
            {
                Log.StreamWriter = new System.IO.StreamWriter(_Path + _File, true);
            }
            catch
            {
                Log.StreamWriter = null;
            }
            return (Log.StreamWriter != null ? true : false);
        }
        private static Boolean CloseWriter()
        {
            if (Log.StreamWriter != null)
            {
                try
                {
                    Log.StreamWriter.Close();
                    Log.StreamWriter.Dispose();
                    Log.StreamWriter = null;
                }
                catch { }
            }
            return (Log.StreamWriter == null ? true : false);
        }
        private static void Write(String Message)
        {
            Console.WriteLine(Message);
            if (Log.Enabled && Log.OpenWriter())
            {
                try
                {
                    Log.StreamWriter.WriteLine("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture) + "] " + Message);
                }
                catch { }
                Log.CloseWriter();
            }
        }
        private static String MethodBase(MethodBase MethodBase)
        {
            return MethodBase.DeclaringType.Namespace + "." + MethodBase.DeclaringType.Name + "." + MethodBase.Name;
        }
    }
}